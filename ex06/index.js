document.addEventListener('DOMContentLoaded', function(){

    (function(){
        var stars = ['Elijah Wood', 'Ian McKellen', 'Orlando Bloom','Ewan McGregor', 'Liam Neeson', 'Natalie Portman', 'Ewan McGregor', 'Billy Bob Thornton', 'Martin Freeman']
        var x = [
            {
              name: "Lord of the Rigs",
              duration: 240,
              starring: [ 'Elijah Wood', 'Ian McKellen', 'Orlando Bloom']
            },
            {
              name: "Star Wars: Episode I - The Phantom Menace",
              duration: 136,
              starring: [ 'Ewan McGregor','Liam Neeson', 'Natalie Portman']
            },
            {
              name: "Fargo",
              duration: 100,
              starring: [ 'Ewan McGregor', 'Billy Bob Thornton', 'Martin Freeman']
            },
            {
              name: "V for Vendetta",
              duration: 150,
              starring: [ 'Hugo Weaving', 'Natalie Portman', 'Rupert Graves']
            },
            {
              name: "Blue Sky",
              duration: 240,
              starring: [ 'Hugo Weaving', 'Elijah Wood', 'Rupert Graves']
            },
            {
              name: "West",
              duration: 136,
              starring: [ 'Hugo Weaving', 'Natalie Portman', 'Rupert Graves', 'Natalie Portman']
            },
        ];
        var duration = [];
        var filter = {
          name: undefined,
          duration: undefined
        }
        
        x.map(function(el){
          duration.push(el.duration);
        });
             
        function outputOptions(arr, clas) {
          var option = null,
              select = document.createElement('select'),
              out = document.querySelector('.filter');
        
          select.classList.add(clas);
        
          arr.map(function(el){
            option = new Option(el, el);
            select.appendChild(option);
          });
        
          out.appendChild(select);
          select.addEventListener('input', function(){
            filterFilms(x);
          });
        }
        
        outputOptions(stars, 'stars');
        outputOptions(duration, 'duration');
        
        function filterFilms(data) {
          var result = [];
          var flag = false;
        
          filter.name = document.querySelector('.stars').value;
          filter.duration = document.querySelector('.duration').value;
          
          data.map(function(el){
            for(let i = 0; i < el.starring.length; i++){
              if(filter.name === el.starring[i]){
                if(parseInt(filter.duration) === el.duration){
                  result.push(el)
                  flag = true;
                  break;
                }
              }
            }
          });
        
          if(!flag){
            document.querySelector('.result').innerHTML = "Ничего не найдено";
          } else {
            return outputFilms(result);
          }
          
        } filterFilms(x);
        
        function outputFilms(data){
          var outputBlock = document.querySelector('.result');
          outputBlock.innerHTML = "";
        
          data.map(function(el){
            var wrappNode = document.createElement('div'),
                nameNode = document.createElement('h1'),
                durationNode = document.createElement('span'),
                starringNode = document.createElement('ul');
            
            nameNode.innerHTML = el.name;
            wrappNode.appendChild(nameNode);
        
            durationNode.innerHTML = el.duration;
            wrappNode.appendChild(durationNode);
        
            el['starring'].map(function(el){
              var liNode = document.createElement('li');
        
              liNode.innerHTML = el;
              starringNode.appendChild(liNode);
            });
        
            wrappNode.appendChild(starringNode);
            outputBlock.appendChild(wrappNode);
          })
        }
    })();
})