window.onload = function() {
  
  function toggleTab(){
    var allTab = document.querySelectorAll('.tab');
    
    document.querySelector('#buttonContainer').onclick = function(e){
      if(e.target.hasAttribute('data-tab')){
        var id = e.target.getAttribute('data-tab');
        
        hideAllTabs();

        document.querySelector('.tab[data-tab="'+id+'"]').classList.add('active'); 
      }   
    };
  
    function hideAllTabs(){
      allTab.forEach(function(el){
        el.classList.remove('active');
      });
    }
  };
  toggleTab();
}

  /*

    Задание 1.

    Написать скрипт который будет будет переключать вкладки по нажатию
    на кнопки в хедере.

    Главное условие - изменять файл HTML нельзя.

    Алгоритм:
      1. Выбрать каждую кнопку в шапке
         + бонус выбрать одним селектором

      2. Повесить кнопку онклик
          button1.onclick = function(event) {

          }
          + бонус: один обработчик на все три кнопки

      3. Написать функцию которая выбирает соответствующую вкладку
         и добавляет к ней класс active

      4. Написать функцию hideAllTabs которая прячет все вкладки.
         Удаляя класс active со всех вкладок

    Методы для работы:

      getElementById
      querySelector
      classList
      classList.add
      forEach
      onclick

      element.onclick = function(event) {
        // do stuff ...
      }

  */