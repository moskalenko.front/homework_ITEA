/*

  Задание:

    1. Написать конструктор объекта комментария который принимает 3 аргумента
    ( имя, текст сообщения, ссылка на аватаку);

    {
      name: '',
      text: '',
      avatarUrl: '...jpg'
      likes: 0
    }
      + Создать прототип, в котором будет содержаться ссылка на картинку по умлочанию
      + В прототипе должен быть метод который увеличивает счетик лайков

    var myComment1 = new Comment(...);

    2. Создать массив из 4х комментариев.
    var CommentsArray = [myComment1, myComment2...]

    3. Созадть функцию конструктор, которая принимает массив коментариев.
      И выводит каждый из них на страничку.

    <div id="CommentsFeed"></div>

*/

document.addEventListener("DOMContentLoaded", function(){
  var commentsArray = [];

  var prot ={
    count: function(){
      this.likes++;
    },
    avaUrl: ".jpg"
  }

  function Comment(name, text, avaUrl){
    this.name = name;
    this.text = text;
    this.avaUrl = avaUrl;
    this.likes = 0; 

    Object.setPrototypeOf( this, prot );
    commentsArray.push(this);
  }

  function outputComments(arr){
    var outField = document.querySelector('#CommentsFeed')
    arr.forEach(el => {
      var template = `
        <div>
        <h1>${el.name}</h1>
        <p>${el.text}</p>
        <img src="${el.avaUrl}">
        <b>${el.likes}</b>
        </div>
      `;

      outField.innerHTML += template;
    });
  }

  var myComment = new Comment('andrey', 'comment', 'img.jpg');
  myComment.count();

  new Comment('andrey1', 'comment1', 'img1.jpg');
  new Comment('andrey2', 'comment2', 'img2.jpg');
  new Comment('andrey3', 'comment3', 'img3.jpg');

  outputComments(commentsArray)

  console.log(myComment);
  console.log(commentsArray)
  

});

