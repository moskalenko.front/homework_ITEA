document.addEventListener('DOMContentLoaded', function(){
    let store = localStorage.getItem("store") ? JSON.parse(localStorage.getItem("store")) : [
        {
            author: "Andrey Moskalenko",
            date: new Date(2011, 10, 1),
            text: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Ullam cumque voluptatibus veritatis reprehenderit qui corporis pariatur odio amet molestiae aperiam. Veritatis, ut eaque unde consequuntur architecto consequatur illo quis natus iusto dolor non voluptates quaerat rem exercitationem tenetur provident dolores impedit reiciendis. Neque provident ipsam fugiat, esse accusamus, unde sunt deserunt libero veritatis cupiditate vero! Perferendis, magni fugiat! Veniam, sapiente!",
            img: 'dist/img/new1.jpg',
            likes: 10,
            comments: [
                {
                    author: "Grinya",
                    date: new Date(2011, 0, 1, 14, 24),
                    text: "comment Lorem ipsum dolor sit amet consectetur adipisicing elit. Ullam cumque voluptatibus veritatis reprehenderit qui corporis"
                },
                {
                    author: "2Grinya",
                    date: new Date(2011, 0, 1, 14, 24),
                    text: "2comment Lorem ipsum dolor sit amet consectetur adipisicing elit. Ullam cumque voluptatibus veritatis reprehenderit qui corporis"
                },
            ]
        },
        {
            author: "Tanya",
            date: new Date(2021, 1, 1),
            text: "tj yk yukuky yu Lorem ipsum dolor sit amet consectetur adipisicing elit. Ullam cumque voluptatibus veritatis reprehenderit qui corporis pariatur odio amet molestiae aperiam. Veritatis, ut eaque unde consequuntur architecto consequatur illo quis natus iusto dolor non voluptates quaerat rem exercitationem tenetur provident dolores impedit reiciendis. Neque provident ipsam fugiat, esse accusamus, unde sunt deserunt libero veritatis cupiditate vero! Perferendis, magni fugiat! Veniam, sapiente!",
            img: 'dist/img/new2.jpg',
            likes: 3,
            comments: [
                {
                    author: "Grinya",
                    date: new Date(2011, 0, 1, 14, 24),
                    text: "comment Lorem ipsum dolor sit amet consectetur adipisicing elit. Ullam cumque voluptatibus veritatis reprehenderit qui corporis"
                },
                {
                    author: "G2rinya",
                    date: new Date(2011, 0, 1, 14, 24),
                    text: "2comment Lorem ipsum dolor sit amet consectetur adipisicing elit. Ullam cumque voluptatibus veritatis reprehenderit qui corporis"
                },
            ]
        },
    ];

    document.getElementById('load-img').addEventListener('change', function(){
        let outField = document.getElementById('preview');

        if(this.value){
            outField.setAttribute('src', this.value);
        }
    });

    document.getElementById('form-constructor').addEventListener('submit', getFormData)

    function getFormData(e){
        e.preventDefault();

        let  data = {
            author: null,
            date: new Date(),
            text: null,
            img: null,
            likes: 0,
            comments: []
        };

        data.author = document.getElementById('const-author').value;
        data.text = document.getElementById('const-text').value;
        data.img = document.getElementById('load-img').value;

        //clear form
        Array.from(this.elements).forEach( function(item){
            if( item.type !== 'submit'){
                item.value = null;
            }
        
        });
        document.getElementById('preview').setAttribute('src', '');

        store.push(data);

        outputPosts();

    }

    function outputPosts(){
        let outputBlock = document.querySelector("#output_posts");

        outputBlock.innerHTML = null;
            
        for (let i = store.length - 1; i >= 0; i--){
            var date = new Date (store[i].date);
            
            outputBlock.innerHTML += `
                <div class="news-list__item news-post" data-id="${i}">
                    <div class="news-post__inner">
                        <p class="news-post__author">Author: <span>${store[i].author}<span></p>
                        <p class="news-post__date">Date: <span>${date.getDay()}.${date.getMonth()}.${date.getFullYear()}</span></p>
                        <p class="news-post__text"> 
                            ${store[i].text}
                        </p>
                        <img class="news-post__img" src="${store[i].img}" alt="">
                        <div class="news-post__btns">
                            <button class="btn-like btn">
                                Likes 
                                <span class="likes-count">${store[i].likes}</span>
                            </button>
                            <button class="btn-show-comment btn">
                                Show Comments
                                <span class="comments_count">${store[i].comments.length}</span>
                            </button>
                            <button class="btn-write-comment btn">Write Comment</button>
                        </div>
                    </div>
                    <div class="constructor__comment">
                        <form action="">
                            <div class="constructor__wrap-input">
                                <input type="text" placeholder="Author">
                            </div>
                            <div class="constructor__wrap-input">
                                <textarea name="" placeholder="Text"></textarea>
                            </div>
                            <button type="submit" class="btn">Send</button>
                        </form>
                    </div>
                    <div class="news-post__comments list-comments">
                    
                    </div>
                </div> 
            `;

        }
        return  (function(){
            outputComments()
        })();

        localStorage.setItem("store", JSON.stringify(store));
    }; outputPosts();

    function outputComments() {
        let posts = document.querySelectorAll('.news-post');

        for (let i = store.length - 1; i >= 0; i--) {
            for (let j = store[i].comments.length - 1; j >= 0; j--){
                let template = `
                    <div class="news-post__comments list-comments">
                        <div class="list-comments__top">
                            <span class="list-comments__author">${store[i].comments[j].author}</span>
                            <span class="list-comments__date">(02.12.2017 14:15)</span>
                        </div>
                        <p class="list-comments__text">
                            Lorem ipsum dolor sit amet consectetur, adipisicing elit. Repellat numquam nostrum est natus molestiae! Quaerat neque harum sit facere magnam?
                        </p>
                    </div>
                `
            }
        }

    }

    function initComment(){
        let btnWriteComment = document.querySelectorAll('.btn-write-comment');

        Array.from(btnShowComment).forEach(function(el){
            el.addEventListener('click', function(e){
                Array.from(document.querySelectorAll('.constructor__comment')).forEach(function(el){
                    el.style.display = 'none';
                });
                this.closest('.news-post').querySelector('.constructor__comment').style.display = 'block';
            });
        })
    }

    function likeCount(){
        let btnsLike = document.querySelectorAll('.btn-like')

        for(let i = 0; i < btnsLike.length; i++){
            btnsLike[i].addEventListener('click', addLike)
        }

        function addLike(){
            let id = this.closest('.news-post').getAttribute('data-id');

            store[id].likes = parseInt(store[id].likes)+1;
            this.querySelector('.likes-count').innerHTML = store[id].likes;

            localStorage.setItem("store", JSON.stringify(store));
        }
    }

});